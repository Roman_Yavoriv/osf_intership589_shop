var _         = require("underscore"),
    mdbClient = require('mongodb').MongoClient;

// Create connection and variable for nav menu
var	catNav;
mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
	var collectionCatNav = db.collection('categories');
	collectionCatNav.find().toArray(function(err, ite){
		catNav = ite;
	}); 
});

// Routes
exports.main = function(req, res) {	
	var	sess = req.session;
		
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');		
		
		collection.find().toArray(function(err, items) {
			res.render("main", {
				// Underscore.js lib 				
				_     : _,
				// Template data
				items : items,
				sess : sess,
				catNav : catNav
			});
			db.close();
		});
	});
};

exports.category = function(req, res) {
	var catName = req.params.cat,
	    sess = req.session;    
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');		
		
		collection.find({"id": catName}).toArray(function(err, items) {
			res.render("category", { 				
				_     : _,							
				items : items,
				catName : catName,
				sess: sess,
				catNav: catNav
			});
			db.close();
		});
	});
};

exports.subcategory = function(req, res) {
    var catName = req.params.cat,
	    subcatName = req.params.subcat,
	    sess = req.session;	
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('categories');		
		
		collection.find({"id": catName }, {"categories": true, "_id": false}).toArray(function(err, items) {
			res.render("subcategory", { 				
				_     : _, 
				items : items,
				subcatName: subcatName,
				catName: catName,
				sess: sess,
				catNav: catNav
			});
			db.close();
		});
	});
};

exports.productList = function(req, res) {
    var catName = req.params.cat,
	    subcatName = req.params.subcat,
	    subsubName = req.params.subsub,
	    sess = req.session;	
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
				
		collection.find().toArray(function(err, items) {
			res.render("productList", { 				
				_     : _, 
				items : items,
				subcatName: subcatName,
				catName: catName,
				subsubName: subsubName,
				sess: sess,
				catNav: catNav
			});
			db.close();
		});
	});
};

exports.pdp = function(req, res) {
    var catName = req.params.cat,
	    subcatName = req.params.subcat,
	    subsubName = req.params.subsub,	
        productIdName = req.params.productId,
        sess = req.session,       
        comment = req.query['comment'],
        stars = req.query['star'];

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products'),
		    collectionComments = db.collection('comments');

		if (comment) {
			collectionComments.insert({
				"commentText" : comment,
				"author": sess.passport.user.username,
				"idProduct": productIdName,
				"stars" : stars	      		
			}, function(){});
		}

		collectionComments.find({"idProduct": productIdName}).toArray(function(err, comm){
			productComments = comm;
		}); 

		collection.find().toArray(function(err, items) {
			res.render("pdp", { 				
				_     : _, 
				items : items,
				subcatName: subcatName,
				catName: catName,
				subsubName: subsubName,
				productIdName : productIdName,
				sess: sess,
				catNav: catNav,
				productComments: productComments
			});
			db.close();
		});
	});
};

exports.login = function(req, res) {	
	var sess = req.session,      
	    userName = req.query['username'],
        userPassword = req.query['password'],
        userEmail = req.query['email'];    
    
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('usersInfo');		

        if (userName) {
			collection.insert({
				"username" : userName,
				"password": userPassword,
	      		"email" : userEmail
			}, function(){});
		}
		
		collection.find().toArray(function(err, items) {
			res.render("login", {				
				_     : _, 
				title : "Log in",
				items : items,
				sess: sess,
				userName: userName,
				catNav: catNav				
			});
			db.close();
		});
	});
};

exports.signup = function(req, res) {	
	var sess = req.session; 	  

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('usersInfo');		
		
		collection.find().toArray(function(err, items) {
			res.render("signup", {				
				_     : _, 
				title : "Sign up",
				items : items,
				sess : sess,
				catNav: catNav				
			});
			db.close();
		});
	});
};

exports.cart = function(req, res) {	
	var sess = req.session;
        catName = req.params.cat,
	    subcatName = req.params.subcat,
	    subsubName = req.params.subsub,	
        productIdName = req.params.productId;   

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');		
		
		collection.find().toArray(function(err, items) {
			res.render("cart", {				
				_     : _, 
				items : items,
				subcatName: subcatName,
				catName: catName,
				subsubName: subsubName,
				productIdName : productIdName,
				sess: sess,
				catNav: catNav				
			});
			db.close();
		});
	});
};