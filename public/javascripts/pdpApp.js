$(document).ready(function() {
	var add = $('#add_to_cart');
	var badge = $('#cart_badge');
	var name = $("#item_name").text();
	var price = $("#item_price").text();
	var imgSrc = $("#main_item_img").attr("src");
	var addedSuccess = $("#added_success");	

	// Watch at the change of currency selector and then changes price
	$("#currency_selector").change(function(){
		$.ajax({
			type: "POST",
			url: 'http://api.fixer.io/latest?base=USD',
			async: true,
			dataType : 'jsonp',   //we may use jsonp for cross origin request
			crossDomain:true,
			success: function(data) {
				var val = $("#currency_selector").val();
				data.rates[val] = data.rates[val] || 1;
				$("#item_price").html((price * data.rates[val]).toFixed(2));
			}
		});               
	});

	// Bind click event to buy button 
	add.on("click", function() {		       
		localStorage.setItem(window.location.pathname, name + "*" + price + "*" + imgSrc);    
		badge.text(localStorage.length);		
		addedSuccess.slideDown(300).delay( 800 ).fadeOut( 400 );
	})
    //  Change href of facebook share button
	var url = window.location;  
    $('.fb-share-button').attr('data-href', url);  

});