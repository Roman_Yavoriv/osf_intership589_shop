(function cart() {
    var list = document.getElementById("cart_list"),
    total = document.getElementById("total"),
    clearCart = document.getElementById("clear_cart"),
    makePurchase = document.getElementById("make_purchase"),
    sum = 0;
   
	for (var i in localStorage) {
        var list_item = document.createElement("li");      // Create a <li> node
		list_item.className = "list-group-item";		

		var listItemName = document.createElement("h2");  // Create item header
		var listItemNameText = document.createTextNode(localStorage[i].split("*")[0]);
        listItemName.appendChild(listItemNameText);
		list_item.appendChild(listItemName);
  
  		var listItemPrice = document.createElement("h3");  // Create item price
		var listItemPriceText = document.createTextNode(localStorage[i].split("*")[1] + "$");
        listItemPrice.appendChild(listItemPriceText);
        list_item.appendChild(listItemPrice);

        var ref = document.createElement("a");   // Create anchor wrapper
        ref.setAttribute('href', i);

        var listItemImg = document.createElement("img");  // Create image
        listItemImg.className = "cartImg";
        listItemImg.setAttribute("src", localStorage[i].split("*")[2]);
        ref.appendChild(listItemImg);        
		list_item.appendChild(ref);

		var rm = document.createElement('button');  // Create remove button
		rm.className = "glyphicon glyphicon-remove";		
		list_item.appendChild(rm);

		// Delete item listener
		rm.addEventListener("click", function(i) {			
			return function () { 				
				localStorage.removeItem(i);
			    location.reload();
			}				
	    }(i));
		
		list.appendChild(list_item); // Append <li> to <ul> with 
		
        sum += +localStorage[i].split("*")[1];
        total.innerHTML = ("Total: " + sum + "$");
	} // End of for loop

	if (localStorage.length === 0) {
        	var emptyMessage = document.createElement("h1");
            var emptyText = document.createTextNode("Your cart is empty :(");
            emptyMessage.appendChild(emptyText);
            list.appendChild(emptyMessage);
            clearCart.style.visibility = "hidden";
            makePurchase.style.visibility = "hidden";
    }
        // Function for clearing cart 
        clearCart.addEventListener("click", function() {
        	var conf = confirm("Are you sure?");
        	
        	if(conf) {
        		localStorage.clear();
        		location.reload();    		
        	}
        })   
})();