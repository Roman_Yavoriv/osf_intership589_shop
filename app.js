// Module dependencies.
var express = require("express");
var favicon = require("serve-favicon");
var morgan = require("morgan");
var http    = require("http");
var path    = require("path");
var routes  = require("./routes");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var cookieParser = require("cookie-parser");
var errorHandler = require("errorhandler");
var session = require("express-session");
var sendgrid  = require('sendgrid')("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");
var app     = express();

// Login Logic
var sess; // Declare session object
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose/');
mongoose.connect('mongodb://localhost/shop');

var Schema = mongoose.Schema;
var UserDetail = new Schema({
      username: String,
      email: String,
      password: String
    }, {
      collection: 'usersInfo'
    });
var UserDetails = mongoose.model('usersInfo', UserDetail);
//  End of Login Logic

// All environments
app.set("port", 80);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

app.use(passport.initialize());
app.use(passport.session());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());
app.use(cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(session({ resave: true, saveUninitialized: true, secret: 'uwotm8' }));
app.use(express.static(path.join(__dirname, "public")));
app.use(errorHandler());
app.use(morgan("dev"));

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();      
}); 

app.get("/", routes.main);
app.get("/login", routes.login);
app.get("/signup", routes.signup);
app.get("/cart", routes.cart);

app.post('/login',
  passport.authenticate('local', {
    successRedirect: '/',    
    failureRedirect: '/login'
  })
);

app.get('/confirmation', function(req, res, next) { 
    sess = req.session;
    sendgrid.send({
      to:       sess.passport.user.email,
      from:     'roman.yavoriv@osf-global.com',
      subject:  'Purchase Confirmation',
      text:     'Hello, " + sess.passport.user.username + "!!! You make purchase at our shop.',
      html:     "<strong>'Hello, " + sess.passport.user.username + "!!! You make purchase at our shop. Please confirm it by calling us: 585-745-12-33.</strong><br><br><i>With best regards, OSF Intern shop-589</i>",
    }, function(err, json) {
      if (err) { return res.send("AAAAAAAHHH!"); }      
      sess.passport.user.confirmation = "sent";     
      res.redirect('/');      
    });    
});

app.get('/logout',function(req,res){
  req.session.destroy(function(err){
    if(err){
      console.log(err);
    }
    else
    {
      res.redirect('/');      
    }    
  })
});

app.use(session({secret: 'sh'}));

app.get("/:cat", routes.category);
app.get("/:cat/:subcat", routes.subcategory);
app.get("/:cat/:subcat/:subsub", routes.productList);
app.get("/:cat/:subcat/:subsub/:productId", routes.pdp);

// Passport local strategy
passport.serializeUser(function(user, done) {
  done(null, user);
});

passport.deserializeUser(function(user, done) {
  done(null, user);
});

passport.use('local', new LocalStrategy(function(username, password, done) {
  process.nextTick(function() {
    UserDetails.findOne({
      'username': username, 
    }, function(err, user) {
      if (err) {
        return done(err);
      }

      if (!user) {
        return done(null, false);
      }

      if (user.password != password) {
        return done(null, false);
      }

      return done(null, user);
    });
  });
}));
// End of Passport local strategy

// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});